import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Title from '../components/ui/Title'
import Colors from '../constants/Colors'
import PrimaryButton from '../components/ui/PrimaryButton'


export default function GameOverScreen({
    roundsNumber,
    userNumber,
    onStartNewGame,
}) {
    return (
    <View style={styles.rootContainer}>
        <Title>Game Over!</Title>
        <View style={styles.imageContainer}>
            <Image style={styles.image}
            source={require('../assets/success.png')} />
        </View>
        <Text  style={styles.summaryText}>Your phone needed <Text style={styles.highlight}>{roundsNumber}</Text>{' '} rounds to guess the number {' '}<Text style={styles.highlight}>{userNumber}</Text> 
        </Text>
        <PrimaryButton onPress={onStartNewGame}>Start New Game</PrimaryButton>
    </View>
)
}

const styles = StyleSheet.create({
    rootContainer: {
        flex: 1,
        padding: 24,
        alignItems: 'center',
        justifyContent: 'center',
    },
    imageContainer: {
        width: 300,
        height: 300,
        borderRadius: 150,
        borderWidth: 3,
        borderColor: Colors.primary800,
        overflow: 'hidden',
        margin: 36,
    },
    image: {
        height: '100%',
        width: '100%',
    },
    highlight: {
        fontFamily: 'opensansbold',
        color: Colors.primary500,
        },
        summaryText: {
        fontFamily: 'opensans',
        fontSize: 24,
        textAlign: 'center',
        marginBottom: 24,
        },
        
})
